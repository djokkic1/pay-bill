import React from 'react';
import { configure } from 'mobx';

import BillStore from '../modules/PayBill/store/bill.store';

configure({
  enforceActions: 'always',
  useProxies: 'never',
});

export const Stores = {
  billStore: BillStore,
};

const StoreContext = React.createContext(Stores);

export const useStore = () => {
  const store = React.useContext(StoreContext);
  if (!store) {
    throw new Error('useStore must be used within a StoreProvider.');
  }
  return store;
};

export default StoreContext;

