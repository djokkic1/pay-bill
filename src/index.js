import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router,
} from "react-router-dom";
import StoreContext, { Stores } from "./store";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Router>
      <StoreContext.Provider value={Stores}>
        <App />
      </StoreContext.Provider>
    </Router>
);

reportWebVitals();
