import React from "react";
import './App.scss';
import {
  Routes,
  Route,
} from "react-router-dom";
import PayBill from "./pages/PayBill/PayBill";

const App = () => {
  return (
      <Routes>
        <Route path="/" element={<PayBill />} />
      </Routes>
  );
};

export default App;
