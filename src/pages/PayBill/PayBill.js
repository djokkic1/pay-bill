import React  from "react";
import './PayBill.scss';
import { useObserver } from 'mobx-react';
import { useStore } from '../../store/index';
import AddPerson from '../../modules/PayBill/UI/AddPerson/AddPerson';
import SingleBill from '../../modules/PayBill/UI/SingleBill/SingleBill';
import TotalBillPerPerson from '../../modules/PayBill/UI/TotalBillPerPerson/TotalBillPerPerson';

const PayBill = () => {

  const { billStore } = useStore();
  const { persons } = billStore;

  return useObserver(() => (
      <div className="pay-bill">
        <div>
          <h1 className="pay-bill__title py-20">Pay Bill</h1>
          <form className="pay-bill__form">
            <AddPerson />
            <SingleBill/>
            {
              persons
              && persons.length > 0
              && <TotalBillPerPerson persons={persons} />
            }
          </form>
        </div>
      </div>
  ));
};

export default PayBill;