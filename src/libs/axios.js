import axios from 'axios';

const config = {
  baseURL: '', // Set the base URL for API requests
};

let headers = {};

// Function to set custom headers
export const setHeaders = (newHeaders) => {
  headers = { ...headers, ...newHeaders };
};

// Function to remove a specific header
export const removeHeader = (header) => {
  delete headers[header];
};

// Create an axios instance with the provided configuration
const http = axios.create(config);

// Interceptor to modify the request before sending
http.interceptors.request.use((config) => {
  for (const header in headers) {
    config.headers[header] = headers[header];
  }
  return config;
});

// Function to create a list of response interceptors (not used in the current code)
export const interceptors = (arg) => [];

// Interceptor to handle response errors
http.interceptors.response.use(
  (response) => response,
  function (error) {
    interceptors.forEach((arg) => (error));
    return Promise.reject(error.response);
  },
);

export default http;
