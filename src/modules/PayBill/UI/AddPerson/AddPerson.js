import React, { useState } from 'react';
import Button from '../../../../components/Button/Button';
import { useStore } from '../../../../store';

const AddPerson = () => {
  const [name, setName] = useState('');
  const [isValid, setIsValid] = useState(true);

  const { billStore } = useStore();

  // Handler for name input change
  const handlePersonNameChange = (e) => {
    const newName = e.target.value;
    setName(newName);
    setIsValid(true); // Clear the validation error when a name is being entered
  };

  // Handler for adding a new person
  const handleAddPerson = async () => {
    if (!name) {
      setIsValid(false); // Set validation error if name is empty
      return;
    }

    setIsValid(true); // Clear the validation error
    const person = {
      name,
      amount: 0,
    };

    await billStore.addPerson(name);
    setName('');
  };

  return (
    <div className="pay-bill__add-person-wrapper">
      <p>Add Person</p>
      <div className="pay-bill__add-person-wrapper__add-person">
        {/* Input for entering person's name */}
        <input
          type="text"
          placeholder="Name..."
          className="py-9 rounded"
          value={name}
          onChange={handlePersonNameChange}
        />
        {/* Button to add the person */}
        <Button buttonText="Add" onClick={handleAddPerson} />
      </div>
      {/* Display validation error message */}
      {!isValid && <small className="text-red-500 my-10">Name cannot be empty!</small>}
    </div>
  );
};

export default AddPerson;