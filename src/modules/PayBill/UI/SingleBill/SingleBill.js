import React, { useState } from 'react';
import './SingleBill.scss';
import Button from '../../../../components/Button/Button';
import { useStore } from '../../../../store';
import { useObserver } from 'mobx-react';
import Select from 'react-dropdown-select';

const SingleBill = () => {
  const { billStore } = useStore();
  const { persons } = billStore;

  const [items, setItems] = useState([{ amount: '', people: [] }]);
  const [isValid, setIsValid] = useState(true);
  const [disabledItems, setDisabledItems] = useState([]);

  // Handler for selecting persons
  const selectedPersonsHandler = (selected, index) => {
    const updatedItems = [...items];
    updatedItems[index].people = selected;
    setItems(updatedItems);
  };

  // Handler for adding a new item
  const addItemHandler = () => {
    const newItem = { amount: 0, people: [] };
    setItems([...items, newItem]);
  };

  // Handler for updating price for target item
  const priceHandler = (e, index) => {
    const price = +e.target.value;
    const updatedItems = [...items];
    updatedItems[index].amount = price;
    setItems(updatedItems);
  };

  // Calculate total and update disabled items
  const calculateTotal = (item, index) => {
    if (!checkIsValid(item)) {
      setIsValid(false);
      return;
    }
    setDisabledItems([...disabledItems, index]);
    setIsValid(true);
    billStore.calculateBill(item);
  };

  // Check if an item is valid
  const checkIsValid = (item) => {
    return item.people.length > 0 && item.amount !== '';
  };

  // Check if an item is disabled
  const checkIsDisabled = (index) => {
    return disabledItems.includes(index);
  };

  return useObserver(() => (
    <div className="single-bill">
      {items.map((item, i) => (
        <div key={i}>
          <div className="single-bill__item">
            {/* Input for Price */}
            <div className="w-2git0 mr-10">
              <p className="my-10">Price</p>
              <input
                type="number"
                placeholder="Price..."
                disabled={checkIsDisabled(i)}
                className="py-10 rounded w-100"
                onChange={(e) => priceHandler(e, i)}
              />
            </div>
            {/* Dropdown select for People */}
            <div className="w-100">
              <p className="my-10">People</p>
              <div className="rounded">
                <Select
                  options={persons}
                  labelField="name"
                  valueField="id"
                  multi
                  className="w-100"
                  disabled={checkIsDisabled(i)}
                  keepSelectedInList={false}
                  onChange={(values) => selectedPersonsHandler(values, i)}
                />
              </div>
            </div>
            {/* Button for calculating total */}
            <div className="w-8 mx-6">
              <Button
                classList="py-9 px-11"
                buttonText="+"
                disabled={checkIsDisabled(i)}
                onClick={() => calculateTotal(item, i)}
              />
            </div>
          </div>
          {/* Display error message when fields are not valid */}
          {!checkIsValid(item) && !isValid && (
            <small className="text-red-500 my-4">Fields cannot be empty!</small>
          )}
        </div>
      ))}
      {/* Button for adding a new item */}
      <Button classList="my-20 py-8" buttonText="Add item" onClick={addItemHandler} />
    </div>
  ));
};

export default SingleBill;
