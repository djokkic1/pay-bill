import React from 'react';
import './TotalBillPerPerson.scss';
import { useObserver } from 'mobx-react';

const TotalBillPerPerson = (props) => {
  return useObserver(() => (
    <div className="total-bill">
      {/* Header section */}
      <div className="total-bill__item total-bill__item__header">
        <h5 className="mx-6">Name</h5>
        <h5 className="mx-6">Iban</h5>
        <h5 className="mx-6">Pay</h5>
      </div>
      {/* List section */}
      <ul className="total-bill__list">
        {props.persons.map((person, i) => (
          <li className="total-bill__item" key={i}>
            <p className="mx-6">{person.name}</p>
            <p className="mx-6">{person.iban}</p>
            <p className="mx-6">{person.amount}$</p>
          </li>
        ))}
      </ul>
    </div>
  ));
};

export default TotalBillPerPerson;
