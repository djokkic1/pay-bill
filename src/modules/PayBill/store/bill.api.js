import http from "../../../libs/axios";

export const createPerson = async (payload) => {
  return http.post('https://63e3e2d765ae49317719e670.mockapi.io/api/v1/users', payload);
}