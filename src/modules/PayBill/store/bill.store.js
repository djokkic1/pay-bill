import { observable, action } from 'mobx';
import { createPerson } from './bill.api';

const billStore = observable({
  persons: [],

  // Action to create a new person
  addPerson: action(async function (username) {
    try {
      const payload = {
        name: username,
      };
      const { data } = await createPerson(payload);
      const person = { ...data, amount: 0 };
      this.persons.push(person);
    } catch (e) {
      console.log(e, 'err');
    }
  }),

  // Action to calculate the bill for each person
  calculateBill: action(function (bill) {
    this.persons.forEach((person) => {
      bill.people.forEach((guest) => {
        if (person.id === guest.id) {
          person.amount += +bill.amount / +bill.people.length;
        }
      });
    });
  }),
});

export default billStore;
