import React from 'react';
import './Button.scss';

const Button = (props) => {
  let classes = 'button'; // Initial CSS class for the button

  if (props.classList) {
    classes += ' ' + props.classList; // Add additional CSS classes if they exist
  }

  if (props.disabled) {
    classes += ' disabled'; // Add the "disabled" class if the button is disabled
  }

  const clickHandler = (e) => {
    e.preventDefault();
    props.onClick();
  };

  return (
    <button
      className={classes}
      onClick={clickHandler}
      disabled={props.disabled}
    >
      {props.buttonText}
    </button>
  );
};

export default Button;
